<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html>

<head>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<title>Logowanie</title>

<style>

	.error {
		color: #ff0000;
	}
</style>

</head>

<body>

	<h2>Zaloguj się do aplikacji</h2>
 
	<form:form method="POST" modelAttribute="loginFormModel">
		<table>
			<tr>
				<td><form:label path="username">Login: </form:label> </td>
				<td><form:input path="username" id="username"/></td>
				<td><form:errors path="username" cssClass="error"/></td>
		    </tr>

			<tr>
				<td><form:label path="password">Hasło: </form:label> </td>
				<td><form:password path="password" id="password"/></td>
				<td><form:errors path="password" cssClass="error"/></td>
		    </tr>
	
			<tr>
				<td><form:label path="role">Rola: </form:label> </td>
				<td>
					<form:select path="role">
						<form:options items="${availableRoles}" />
					</form:select>
				</td>
		    </tr>
	
			<tr>
				<td colspan="3">
					<input type="submit" value="Zaloguj się"/>
				</td>
			</tr>
		</table>
	</form:form>
	<br/>
	<br/>
	Nie masz konta? <a href="<c:url value='/register' />">Zarejestruj się.</a>
</body>
</html>