package bsk.springmvc.model;

import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Size;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name="app_user")
public class User {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Size(min=3, max=50)
	@Column(name = "username", unique=true, nullable = false)
	private String username;

	@NotEmpty
	@Column(name = "password", nullable = false)
	private byte[] password;

	@ManyToOne
	@JoinTable(
			name="user_role",
			joinColumns=@JoinColumn(name="user_id", referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="role_id", referencedColumnName="id"))
	private Role current_role;

	@ManyToMany
	@JoinTable(
			name="user_role",
			joinColumns=@JoinColumn(name="user_id", referencedColumnName="id"),
			inverseJoinColumns=@JoinColumn(name="role_id", referencedColumnName="id"))
	private List<Role> roles = new ArrayList<Role>();

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

	public String getUsername() {
		return username;
	}
	public void setUsername(String username) {
		this.username = username;
	}

	public boolean isPasswordCorrect(String password){
		try {
			return this.password == this.hashPassword(password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return false;
	}
	public byte[] getPassword() {
		return password;
	}
	public void setPassword(String password) {
		try {
			this.password = this.hashPassword(password);
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
	}

	public List<Role> getRoles() {
		return this.roles;
	}
	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public Role getCurrentRole() {
		return current_role;
	}
	public void setCurrentRole(Role role) {
		if (roles.contains(role))
			this.current_role = role;
	}

	private byte[] hashPassword(String password) throws NoSuchAlgorithmException {
		MessageDigest sha256 = MessageDigest.getInstance("SHA-256");
		byte[] passBytes = password.getBytes();
		return sha256.digest(passBytes);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + id;
		result = prime * result + ((username == null) ? 0 : username.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof User))
			return false;
		User other = (User) obj;
		if (id != other.id)
			return false;
		if (username == null) {
			if (other.username != null)
				return false;
		} else if (!username.equals(other.username))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "User [id=" + id + ", username=" + username + "]";
	}

}
