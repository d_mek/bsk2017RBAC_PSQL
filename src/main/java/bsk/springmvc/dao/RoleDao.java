package bsk.springmvc.dao;

import bsk.springmvc.model.Role;

import java.util.List;

public interface RoleDao {

	Role getById(int id);
	Role create(String name, String description);
	List<Role> getAllRoles();

}
