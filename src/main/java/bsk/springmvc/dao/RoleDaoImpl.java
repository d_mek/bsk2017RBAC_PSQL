package bsk.springmvc.dao;

import bsk.springmvc.model.Employee;
import bsk.springmvc.model.Role;
import org.hibernate.Criteria;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("roleDao")
public class RoleDaoImpl extends AbstractDao<Integer, Role> implements RoleDao {

	public Role getById(int id) {
		return getByKey(id);
	}

	public Role create(String name, String description){
		Role new_role = new Role();
		new_role.setName(name);
		new_role.setDescription(description);
		persist(new_role);
		return new_role;
	}

	public List<Role> getAllRoles() {
		Criteria criteria = createEntityCriteria();
		return (List<Role>) criteria.list();
	}

}
