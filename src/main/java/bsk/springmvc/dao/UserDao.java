package bsk.springmvc.dao;

import bsk.springmvc.model.User;

public interface UserDao {

	User getById(int id);
}
