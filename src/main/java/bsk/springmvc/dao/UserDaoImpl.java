package bsk.springmvc.dao;

import bsk.springmvc.model.Role;
import bsk.springmvc.model.User;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository("userDao")
public class UserDaoImpl extends AbstractDao<Integer, User> implements UserDao {

	public User getById(int id) {
		return getByKey(id);
	}
}
