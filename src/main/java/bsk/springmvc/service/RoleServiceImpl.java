package bsk.springmvc.service;

import bsk.springmvc.dao.RoleDao;
import bsk.springmvc.model.Role;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service("roleService")
@Transactional
public class RoleServiceImpl implements RoleService {

	@Autowired
	private RoleDao dao;
	
	public Role getById(int id) {
		return dao.getById(id);
	}

	public Role create(String name, String description) {
		return dao.create(name, description);
	}

	public List<String> getAllNames() {
		List<Role> roles = dao.getAllRoles();
		List<String> roles_names = new ArrayList<String>();
		for (Role role : roles){
			roles_names.add(role.getName());
		}
		return roles_names;
	}

}
