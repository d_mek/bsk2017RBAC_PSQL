package bsk.springmvc.service;

import bsk.springmvc.model.Role;

import java.util.List;

public interface RoleService {

	Role getById(int id);
	Role create(String name, String description);
	List<String> getAllNames();
}
