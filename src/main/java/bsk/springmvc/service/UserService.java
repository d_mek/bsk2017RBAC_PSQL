package bsk.springmvc.service;

import bsk.springmvc.model.Role;
import bsk.springmvc.model.User;

import java.util.Set;

public interface UserService {

	User findById(int id);
	
	void updateUserPassword(User user, String oldPassword, String newPassword);

	Set<Role> getUserRoles(int id);
}
