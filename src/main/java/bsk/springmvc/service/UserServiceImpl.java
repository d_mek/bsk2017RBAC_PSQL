package bsk.springmvc.service;

import bsk.springmvc.dao.UserDao;
import bsk.springmvc.model.Role;
import bsk.springmvc.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.Set;

@Service("userService")
@Transactional
public class UserServiceImpl implements UserService {

	@Autowired
	private UserDao dao;
	
	public User findById(int id) {
		return dao.getById(id);
	}

	public void updateUserPassword(User user, String oldPassword, String newPassword) {
		User entity = dao.getById(user.getId());
		if(entity!=null && user.isPasswordCorrect(oldPassword)){
			entity.setPassword(newPassword);
		}
	}

	public Set<Role> getAllUsers() {
		throw new NotImplementedException();
	}

	public Set<Role> getUserRoles(int id) {
		throw new NotImplementedException();
	}
}
