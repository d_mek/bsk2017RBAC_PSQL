--
-- PostgreSQL database dump
--

-- Dumped from database version 9.6.2
-- Dumped by pg_dump version 9.6.2

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: plpgsql; Type: EXTENSION; Schema: -; Owner: 
--

CREATE EXTENSION IF NOT EXISTS plpgsql WITH SCHEMA pg_catalog;


--
-- Name: EXTENSION plpgsql; Type: COMMENT; Schema: -; Owner: 
--

COMMENT ON EXTENSION plpgsql IS 'PL/pgSQL procedural language';


SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- Name: app_user; Type: TABLE; Schema: public; Owner: bsk2017user
--

CREATE TABLE app_user (
    id integer NOT NULL,
    password bytea NOT NULL,
    username character varying(50) NOT NULL
);


ALTER TABLE app_user OWNER TO bsk2017user;

--
-- Name: app_user_id_seq; Type: SEQUENCE; Schema: public; Owner: bsk2017user
--

CREATE SEQUENCE app_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE app_user_id_seq OWNER TO bsk2017user;

--
-- Name: app_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bsk2017user
--

ALTER SEQUENCE app_user_id_seq OWNED BY app_user.id;


--
-- Name: role; Type: TABLE; Schema: public; Owner: bsk2017user
--

CREATE TABLE role (
    id integer NOT NULL,
    description character varying(255),
    name character varying(50) NOT NULL
);


ALTER TABLE role OWNER TO bsk2017user;

--
-- Name: role_id_seq; Type: SEQUENCE; Schema: public; Owner: bsk2017user
--

CREATE SEQUENCE role_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE role_id_seq OWNER TO bsk2017user;

--
-- Name: role_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bsk2017user
--

ALTER SEQUENCE role_id_seq OWNED BY role.id;


--
-- Name: user_role; Type: TABLE; Schema: public; Owner: bsk2017user
--

CREATE TABLE user_role (
    id integer NOT NULL,
    role_id integer,
    user_id integer NOT NULL
);


ALTER TABLE user_role OWNER TO bsk2017user;

--
-- Name: user_role_user_id_seq; Type: SEQUENCE; Schema: public; Owner: bsk2017user
--

CREATE SEQUENCE user_role_user_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE user_role_user_id_seq OWNER TO bsk2017user;

--
-- Name: user_role_user_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: bsk2017user
--

ALTER SEQUENCE user_role_user_id_seq OWNED BY user_role.user_id;


--
-- Name: app_user id; Type: DEFAULT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY app_user ALTER COLUMN id SET DEFAULT nextval('app_user_id_seq'::regclass);



--
-- Name: role id; Type: DEFAULT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY role ALTER COLUMN id SET DEFAULT nextval('role_id_seq'::regclass);


--
-- Name: user_role user_id; Type: DEFAULT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY user_role ALTER COLUMN user_id SET DEFAULT nextval('user_role_user_id_seq'::regclass);


--
-- Data for Name: app_user; Type: TABLE DATA; Schema: public; Owner: bsk2017user
--

COPY app_user (id, password, username) FROM stdin;
\.


--
-- Name: app_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bsk2017user
--

SELECT pg_catalog.setval('app_user_id_seq', 1, false);




--
-- Data for Name: role; Type: TABLE DATA; Schema: public; Owner: bsk2017user
--

COPY role (id, description, name) FROM stdin;
\.


--
-- Name: role_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bsk2017user
--

SELECT pg_catalog.setval('role_id_seq', 1, false);


--
-- Data for Name: user_role; Type: TABLE DATA; Schema: public; Owner: bsk2017user
--

COPY user_role (id, role_id, user_id) FROM stdin;
\.


--
-- Name: user_role_user_id_seq; Type: SEQUENCE SET; Schema: public; Owner: bsk2017user
--

SELECT pg_catalog.setval('user_role_user_id_seq', 1, false);


--
-- Name: app_user app_user_pkey; Type: CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT app_user_pkey PRIMARY KEY (id);



--
-- Name: role role_pkey; Type: CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY role
    ADD CONSTRAINT role_pkey PRIMARY KEY (id);


--
-- Name: app_user uk_3k4cplvh82srueuttfkwnylq0; Type: CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY app_user
    ADD CONSTRAINT uk_3k4cplvh82srueuttfkwnylq0 UNIQUE (username);


--
-- Name: role uk_8sewwnpamngi6b1dwaa88askk; Type: CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY role
    ADD CONSTRAINT uk_8sewwnpamngi6b1dwaa88askk UNIQUE (name);



--
-- Name: user_role user_role_pkey; Type: CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT user_role_pkey PRIMARY KEY (user_id);


--
-- Name: user_role fk_apcc8lxk2xnug8377fatvbn04; Type: FK CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_apcc8lxk2xnug8377fatvbn04 FOREIGN KEY (user_id) REFERENCES app_user(id);


--
-- Name: user_role fk_it77eq964jhfqtu54081ebtio; Type: FK CONSTRAINT; Schema: public; Owner: bsk2017user
--

ALTER TABLE ONLY user_role
    ADD CONSTRAINT fk_it77eq964jhfqtu54081ebtio FOREIGN KEY (role_id) REFERENCES role(id);


--
-- PostgreSQL database dump complete
--

