INSERT INTO role (name, description)
VALUES ('student', 'Student role.');

INSERT INTO app_user (username, password)
VALUES ('example_student', '9f86d081884c7d659a2feaa0c55ad015a3bf4f1b2b0b822cd15d6c15b0f00a08');

INSERT INTO user_role
  SELECT app_user.id, role.id FROM role, app_user WHERE name='student' AND username='example_student';
