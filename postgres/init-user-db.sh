#!/bin/bash

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" <<-EOSQL
    CREATE USER bsk2017user PASSWORD 'abc123';
    CREATE DATABASE bsk2017db;
    GRANT ALL PRIVILEGES ON DATABASE bsk2017db TO bsk2017user;
EOSQL

psql -v ON_ERROR_STOP=1 --username "$POSTGRES_USER" -f /usr/src/createAppUserAndRoleTables.sql bsk2017db

psql -v ON_ERROR_STOP=1 --username bsk2017user -f /usr/src/insertExampleAppUsersAndRoles.sql bsk2017db